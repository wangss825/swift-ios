//
//  Card.swift
//  CST207Assign2
//
//  Created by Dan Liu on 2019-03-29.
//  Copyright © 2019 cst207. All rights reserved.
//

/*
 Purpose: set up the card class
 */
import Foundation
public struct Card: CustomStringConvertible
{
    var rank: Rank
    var suit: Suit
    public var description: String
    {
        //get the four suit's emoji
        let Heart = "\u{2665}"
        let Spade = "\u{2660}"
        let diamond = "\u{2666}"
        let club = "\u{2663}"
        
        switch suit
        {
        case Suit.heart:
            return "\(rank) of \(Heart)"
        case Suit.clubs:
            return "\(rank) of \(club)"
        case Suit.diamonds:
            return "\(rank) of \(diamond)"
        case Suit.spades:
            return "\(rank) of \(Spade)"
        default:
            "something wrong "
            
        }
        return " \(rank) of \(suit)"
        
    }
    
    
}
