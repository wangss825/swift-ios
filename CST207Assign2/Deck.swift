//
//  Deck.swift
//  CST207Assign2
//
//  Created by Dan Liu on 2019-03-29.
//  Copyright © 2019 cst207. All rights reserved.
//
/*
 Purpose: set up the Dec class
 */


import Foundation
public class Deck
{
    
     var cards: [Card]=[Card](); //card array
    //initialize the card , all togethe 52 cards
    public init()
    {
        for i  in 1...4
        {
            for  j in 2...14
            {
                var card = Card(rank: Rank.Ace, suit: Suit.diamonds)
                card.suit = Suit(rawValue: i)!
                card.rank = Rank(rawValue: j)!
                cards.append(card)
            }
        }
        //shuffle the card array
        self.cards.shuffle()
    }

    //shffle the cards array
    public func shuffle()
    {

        for _ in 1...1000
        {
            //need the reandom number between 0 and 51
            var randomNum1 = Int(arc4random_uniform(UInt32(52)))
            //randomNum1-=1;
            var randomNum2 = Int(arc4random_uniform(UInt32(52)))
            //randomNum2-=1;
            self.cards.swapAt(randomNum1, randomNum2)
            
        }
    }
    
    //get an optional card
    public func getNextCard()-> Card!
    {
        //if the cards array is not empty
        if cards.count > 0
        {
            var randomNum = Int(arc4random_uniform(UInt32(53)))
            randomNum-=1;
            return self.cards[randomNum]
        }
        //if the card is empty
        else
        {
            return nil
        }


    }
 }
