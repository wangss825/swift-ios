//
//  Rank.swift
//  CST207Assign2
//
//  Created by Dan Liu on 2019-03-29.
//  Copyright © 2019 cst207. All rights reserved.
//

/*
 Purpose: enum variable and some essential method
 */

import Foundation
//emum for the card's rank
enum Rank:Int
{
    case two = 2, three, four, five, six, seven, eight, nine, ten, Jack, Queen, King, Ace
    // get the rank array
    public func rankArray() ->[String]
    {
        var rank = [String]();
        rank += ["two","three", "four","five", "six", "seven", "eight","nine", "ten", "Jack","Queen","King", "Ace"]
        return rank
    }
    
}
//enum for the suit
enum Suit:Int
{
    case heart = 1, spades, diamonds, clubs
    //get the suit array
    func suitArray()->[String]
    {
        var suit = [String]();
        suit += ["heart", "spades", "diamonds", "clubs"]
        return suit
    }
}
//get the user's input from keyboard
public func input()->String
{
    print("Welcome to Command line game")
    var inputName: String!
    print("Please enter you name:")
    inputName = readLine()!
    return inputName
    
}

