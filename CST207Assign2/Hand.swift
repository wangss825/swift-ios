//
//  Hand.swift
//  CST207Assign2
//
//  Created by Dan Liu on 2019-03-31.
//  Copyright © 2019 cst207. All rights reserved.
//

/*
 Purpose: set up the hand class
 */
import Foundation

public class Hand
{
     var cards: [Card] = [Card]() //card array to hold the cards come from deal card
    var discard: [Card] = [Card]()//discard array to hold the card come from the checking round
    var count: Int { return  cards.count + discard.count}//all the card includ the cards and discard
    
    /*
     Purpose: To get the next card to play
    */
    public func getNextCard() ->Card?
    {
        //if the cards array has card, get card from this array
        if self.cards.count > 0
        {
               return self.cards[0]
        }
        //if the cards array is empty and the discard card array has card, to get card from this array
        else if self.cards.count == 0 && self.discard.count > 0
        {
              return self.discard[0]
        }
        else
        {
            //both empty, return nil
            return nil
        }
    }
    
    /*
     Purpose: add card to the discard array
    */
    public func addToDiscard( card :Card )
    {
        self.discard.append(card)
    }
    /*
     Purpose: after get next card, should delete the card from the card array or discard array
     */
    public func deleteCard(card: Card)
    {
        var  i : Int = 0
        //check if the card is in the card array
        for currentCard in cards
        {
            //check if the card is the same as the current card in the array
            if currentCard.rank == card.rank && currentCard.suit == card.suit
            {
                //if same, delete it by remove the last one to this position
                cards[i].rank = cards[cards.count-1].rank
                cards[i].suit = cards[cards.count-1].suit
                //remove the last one in the array
                cards.removeLast()
                break
            }
           i += 1
        }
        //check the card is in the discard array
        for currentCard in discard
        {
            //check if the card is the same as the current card in the discard array
            if currentCard.rank == card.rank && currentCard.suit == card.suit
            {
                //delete by substitute this postition's postion as the last card in the array
                discard[i].rank = discard[discard.count-1].rank
                discard[i].suit = discard[discard.count-1].suit
                //remove the last one
                discard.removeLast()
                break
            }
            i += 1
        }

        
    
    }
    
}
