//
//  PlayWar.swift
//  CST207Assign2
//
//  Created by Dan Liu on 2019-03-31.
//  Copyright © 2019 cst207. All rights reserved.
//

/*
 Purpose: This class is for all the process of playing the game. from deal card to decide the final
 winner
 */

import Foundation


public class PlayWar
{
 
    var cardArray:[Card]=[Card]() //temp card array to hold the card for tie
    private var human:Player
    private var computer:Player
    public init()
    {
        //get player name from input
        var playerName = input()
        //set up hands for players
        var humanHand :Hand = Hand()
        var computerHand : Hand = Hand()        
       //set up the player
        self.human = Player(name: playerName, hand: humanHand)
        self.computer = Player(name: "computer", hand: computerHand)
        
     }
    
    /*
     Purpose: to check the winner for every round
    */
    public func checkForWinner( humanCard : Card!, computerCard: Card!)->Bool
    {
        //if human's card greater than computer's card, than the human is the winner for this round
        if humanCard.rank.rawValue > computerCard.rank.rawValue
        {
            print("\(human.getName()) win this round")
            //Human's card add to the discard array
            human.hand.addToDiscard(card:computerCard)
            //computer's card still put to the human's discard array
            computer.hand.deleteCard(card: computerCard)
            //if the last round is tie, there will be some card have not decided, I have
            //put all these card in a array, here just give the winner all these card
            if self.cardArray.count > 0
            {
   
                for i in cardArray
                {
                    human.hand.addToDiscard(card: i)
                }
                self.cardArray.removeAll()
            }
            return true
        }
        //if any play's card is empty, go to playWar to decide the final winner
        if humanCard == nil || computer == nil
        {
 
            playWar()
            return false
        }
        //if computer's card is greater than the human, computer win this round, same as the first if in
        //this function
        if computerCard.rank.rawValue > humanCard.rank.rawValue
        {
            print("\(computer.getName()) win this round")
            computer.hand.addToDiscard(card: humanCard)
            human.hand.deleteCard(card: humanCard)
            
            if self.cardArray.count > 0
            {
 
                for i in cardArray
                {
                    computer.hand.addToDiscard(card: i)
                }

                self.cardArray.removeAll()
            }

            return true
        }
        //tie
        if computerCard.rank.rawValue == humanCard.rank.rawValue
        {
           //put computer's and human's card to carArray
            cardArray.append(computerCard)
            cardArray.append(humanCard)
            
            //delete the card from the computer and human
            human.hand.deleteCard(card: humanCard)
            computer.hand.deleteCard(card: computerCard)
            
            print("Tie!!  Draw 3 cards facedown, then 1 up")
        
            //the carArray will add all these card for the future use
            self.cardArray += tie()
            
            //continue game
            playRound()
            
            return false
        }
        return true
        

    
    }
    
    /*
     Purpose:if tie, the players give 3 cards, and store in an array.
    */
    public func tie()->[Card]
    {
        var cardTempArray:[Card]=[Card]()

        //check if the two player both have more than 3 cards
        //if it is, can give out the cards
        //otherwise, can go to playWar to get the final result
        if human.hand.count >= 3 && computer.hand.count >= 3
        {
            print("Each player played a card facedown")
            var hCard1 = human.hand.getNextCard()
            var cCard1 = computer.hand.getNextCard()
            human.hand.deleteCard(card: hCard1!)
            computer.hand.deleteCard(card: cCard1!)
            print("Each player played a card facedown")
            var hCard2 = human.hand.getNextCard()
            var cCard2 = computer.hand.getNextCard()
            human.hand.deleteCard(card: hCard2!)
            computer.hand.deleteCard(card: cCard2!)
            print("Each player played a card facedown")
            var hCard3 = human.hand.getNextCard()
            var cCard3 = computer.hand.getNextCard()
            human.hand.deleteCard(card: hCard3!)
            computer.hand.deleteCard(card: cCard3!)
            cardTempArray.append(hCard1!)
            cardTempArray.append(hCard2!)
            cardTempArray.append(hCard3!)
            cardTempArray.append(cCard1!)
            cardTempArray.append(cCard2!)
            cardTempArray.append(cCard3!)

        }
        else
        {
            playWar()
        }
        return cardTempArray
    }
    /*
     Purpose: dealcard, only for the first time to deal card,
    */
    public func dealCards()
    {
        //check if the player have card, it not, deal card,
        //otherwise, don't
        if (human.hand.count + computer.hand.count) == 0
        {
            
            var deck = Deck()
            //shuffle the cards
            deck.shuffle()
            //every player get 26 cards
            var number : Int = 1
            for i in stride(from: 0, to: 51, by: 2)
            {
                human.hand.cards.append(deck.cards[i])
                computer.hand.cards.append(deck.cards[i+1])
                number += 1;
            }
        }
    }
    /*
     Purpose: decide the final winner condition otherwise go on the game
     */
    public func playWar()
    {
        //deal cards
        dealCards()
        print("***********************************")
        print(human.description)
        print(computer.description)
        print("************************************")
        print("Please press enter to continue")
        readLine()!
        
        //check the cards condition
        if human.hand.count > 0 && computer.hand.count <= 0
        {
            //human have card, computer have not card , human win
            print("\(human.getName()) is the winner !!!)")
            exit(0)
        }
        else if computer.hand.count > 0 &&  human.hand.count <= 0
        {
            //computer have card, human have no card , computer win
            print("\(computer.getName()) is the winner!!!")
            exit(0)
        }
        //both human and computer have card , go to playRound
        else if  human.hand.count > 0  &&  computer.hand.count > 0
        {
                playRound()
        }
        
}
    
    /*
     Purpose: player give card, and go to checkForWinner method to check the winner for one round
     Then continue the game by go to playWar()
     */
    public func playRound()
    {
        //if else statemnt - in case that after tie one player have no card
        //only any of the player have card the game can go on continuely, other wise just go to
        //playWar to get the final result
        if human.hand.count > 0 && computer.hand.count > 0
        {
            var hName = human.getName()
            var hCard = human.hand.getNextCard()!
            print("\(hName) played \(hCard)")
            
            var cName = computer.getName()
            var cCard = computer.hand.getNextCard()!
            print("\(cName) played \(cCard)")
            
            //check for winner for one round
            checkForWinner(humanCard: hCard, computerCard: cCard)
            
            playWar()

        }
       // at least one side have no card
        else
        {
            playWar()
        }

        
        
    }
}
