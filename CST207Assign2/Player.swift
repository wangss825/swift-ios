//
//  Player.swift
//  CST207Assign2
//
//  Created by Dan Liu on 2019-03-31.
//  Copyright © 2019 cst207. All rights reserved.
//
/*
 Purpose: Set up the player class
 */
import Foundation
public class Player: CustomStringConvertible
{
    public  var name: String
    public var hand: Hand
    public var description: String
    {
            var totalCard = self.hand.count
            return "\(name) has \(totalCard) cards"
    }
    public init(name: String , hand: Hand)
    {
        self.name = name
        self.hand = hand
     }
    public func getName()->String
    {
        return self.name
    }
    //get next card to play
    public func getNextCard()->Card?
    {
          return  hand.getNextCard()
        
    }
    //add the card to the discard array
    public func addCard(card: Card)
    {
        hand.addToDiscard(card: card)
    }
    
    
    
}
